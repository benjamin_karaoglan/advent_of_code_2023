# Advent of Code 🎄

## What is Advent of Code?

Advent of Code is an annual coding event that takes place during the Advent season, from December 1st to December 25th. It consists of a series of daily programming puzzles that participants solve using their choice of programming language.

## Why is Advent of Code useful?
🧩 **Problem Solving**: Advent of Code offers a diverse range of puzzles that test our problem-solving skills and algorithmic thinking. It helps improve our logical reasoning abilities and enhances our coding skills.

💡 **Learning Opportunities**: The puzzles in Advent of Code often introduce new concepts, algorithms, and data structures. As we solve these challenges, we'll learn new techniques and expand our programming knowledge.

🌟 **Community and Collaboration**: Advent of Code has a vibrant community of developers, so we can share our solutions, discuss approaches, and learn from others. It's a great opportunity to connect with like-minded individuals.

## What we'll find in this repository

For each day of the [AOC 2023](https://adventofcode.com/2023), you will find a corresponding directory containing:

- `main.py`  containing the script to solve this puzzle
- `input.txt` containing an short example of input, for part 1 and 2