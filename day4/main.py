import numpy as np
import regex as re
from collections import defaultdict


def read_input(input_path: str) -> list:
    """Opens and return an input string as a list containing the lines"""
    with open(input_path, "r") as f:
        input = f.readlines()
    return input


def points(count: int) -> int:
    """Recursive function that given a count, gives back the number of
    points"""
    if count == 1:
        return count
    elif count == 0:
        return count
    else:
        return points(count - 1) * 2


def points_calc(win: list, play: list) -> int:
    """Given a grid (win and play), returns the equivalent points"""
    count = 0
    for num in play:
        if num in win:
            count += 1
    return points(count)


def solution_1(parsed_input: list) -> int:
    """Part 1 of the solution"""
    # For each grid/game, we calculate the number of points of a given grid
    counts = {}
    for i, (win, play) in enumerate(parsed_input):
        game_points = points_calc(win, play)
        counts[i] = game_points
    return sum(counts.values())


def solution_2(parsed_input: list) -> int:
    """Part 2 of the solution"""
    # For each grid/game, we keep track of the instances of each card using
    # a dict of deafult value 1. Then, for each card, we multiply the winned
    # copy by the number of instances of the current card and add them to
    # the instances
    game_instance = defaultdict(lambda: 1)
    for i, (win, play) in enumerate(parsed_input):
        count = 0
        for num in play:
            if num in win:
                count += 1
        for game in range(i + 1, i + count + 1):
            game_instance[game] += 1 * game_instance[i]
    return sum(game_instance.values())


if __name__ == "__main__":
    input = read_input("/home/benjamin_karaoglan/advent_of_code_2023/day4/input.txt")
    # We parse our input by creating two lists: the win and play, each having
    # the numbers of the grids of the cards
    parsed_input = []
    for line in input:
        grid = line.strip().split(":")[1].strip().split("|")
        win, play = grid[0].strip(), grid[1].strip()
        win = [int(num) for num in re.findall(r"\d+", win)]
        play = [int(num) for num in re.findall(r"\d+", play)]
        parsed_input.append((win, play))
    print(solution_1(parsed_input))
    print(solution_2(parsed_input))
