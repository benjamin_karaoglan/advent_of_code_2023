import numpy as np
from collections import defaultdict

# Dict to check the actual count of balls
actual = {"blue": 14, "red": 12, "green": 13}


def read_input(input_path: str) -> list:
    """Opens and return an input string as a list containing the lines"""
    with open(input_path, "r") as f:
        input = f.readlines()
    return input


def max_balls_count(game: list) -> dict:
    """For a game, counts the max number of balls that were picked for each
    color"""
    balls = defaultdict(int)
    # For each picks in a game, we iterate (picks are subsets of cubes)
    for picks in game:
        # For each pair color/count
        for pick in picks.split(","):
            # We seperate the count from the color, and turn the count in
            # int
            count, color = pick.strip().split()
            count = int(count)
            # We check if, for this color, the current count is the max
            if balls[color] < count:
                balls[color] = count
    # Finally we return the dict containing the max count for each color
    return balls


def solution_1(parsed_input: list) -> int:
    """Part 1 solution"""
    # Empty list to keep track of games that are valid
    valid_games = []
    # We iterate through each game
    for i, game in parsed_input.items():
        # For each game, we create an empty dict to keep track of the max
        # amount of balls for each color
        balls = max_balls_count(game)
        # Then, we check that for each of the real counts obtained in the
        # actual dict that the current count doesn't exceed
        check = 0
        for color, real_count in actual.items():
            if balls[color] <= real_count:
                check += 1
        # If for the three colors we met the criteria, the number of the game
        # is valid
        if check == 3:
            valid_games.append(int(i))
    # Finally we return the sum of the index of the games
    return sum(valid_games)


def solution_2(parsed_input: list) -> int:
    """Part 2 solution"""
    # We keep track of each games power
    powers = []
    # For each game, we iterate
    for game in parsed_input.values():
        # We get the dict having the max count for each color
        balls = max_balls_count(game)
        # We compute the game's power my multiplying the max counts together
        powers.append(np.prod(list(balls.values())))
    # Finally, we return the sum of all the games powers
    return sum(powers)


if __name__ == "__main__":
    input_list = read_input(
        "/home/benjamin_karaoglan/advent_of_code_2023/day2/input.txt"
    )
    parsed_input = {
        line.split(":")[0].split()[1]: line.split(":")[1].strip().split(";")
        for line in input_list
    }
    print(solution_1(parsed_input))
    print(solution_2(parsed_input))
