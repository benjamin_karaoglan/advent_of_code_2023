import regex as re
from collections import defaultdict


def read_input(input_path: str) -> list:
    """Opens and return an input string as a list containing the lines"""
    with open(input_path, "r") as f:
        input = f.readlines()
    return input


def solution_1(nums: dict, syms: dict) -> int:
    """Part 1 of solution"""

    # Empty list to keep track of the parts numbers
    part_num = []
    # For each line we iterate through the numbers, and keep track of the line
    # index
    for i, lines in nums.items():
        # For each number in a line
        for num_pos in lines:
            # We keep track of the number, and its start and end position/index
            num, pos = num_pos[0], num_pos[1:]
            # If for the current line index, there exist a symbol on the
            # previous, current or next line
            if i in syms or i - 1 in syms or i + 1 in syms:
                # We get the corresponding symbols positions for the existing
                # lines
                sym_pos = [syms.get(i), syms.get(i - 1), syms.get(i + 1)]
                # We introduce a break flag for later because of nested loops
                break_flag = False
                # For each symbols positions on these lines
                for indexes in sym_pos:
                    if indexes is not None:
                        # We check if the symbol is right next to the part
                        for index in indexes:
                            if index[1] >= min(pos) - 1 and index[1] < max(pos) + 1:
                                # If it is, we append the number to our parts,
                                # and break
                                part_num.append(num)
                                break_flag = True
                                break
                    if break_flag:
                        # We break a second time to make sure that the numbers
                        # are not counted twice
                        break
    # Finally, we return the sum of all parts
    return sum(part_num)


def solution_2(nums: dict, syms: dict) -> int:
    """Part 2 of solution"""
    # Empty list to keep tracks of the gears ratio
    gears_ratio = []
    # For each line, and each symbols on these lines
    for i, sym_pos in syms.items():
        # We keep track of the symbol and it's index on the line
        for sym, pos in sym_pos:
            # If the symbol is a gear (*) and that there are numbers in the
            # current,above or bellow lines
            if sym == "*" and (i in nums or i - 1 in nums or i + 1 in nums):
                # We keep track of the parts next to this gear
                parts = []
                # We get all numbers that are on the adjascent lines
                num_pos = [nums.get(i), nums.get(i - 1), nums.get(i + 1)]
                # For each of the numbers
                for indexes in num_pos:
                    if indexes is not None:
                        # We check if the indexes are adjascent to the sym
                        # If they are, we store them in the parts
                        for index in indexes:
                            if index[1] - 1 <= pos and index[2] >= pos:
                                parts.append(index[0])
                # If for a gear there are two parts, we compute their gear
                # ratio and store it
                if len(parts) == 2:
                    gears_ratio.append(parts[0] * parts[1])
    # Finally, we return the sum of all gear ratios
    return sum(gears_ratio)


if __name__ == "__main__":
    input_list = read_input(
        "/home/benjamin_karaoglan/advent_of_code_2023/day3/input.txt"
    )

    # We set up two dict with lists as defaults values to keep track of
    # the nums and symbols positions on each line
    nums = defaultdict(list)
    syms = defaultdict(list)
    # For each line, we create a key in the dict that contains the index
    # of the line and that has for values: the num, start position end
    # position for the nums ; the symbol, position for the symbols
    for i, line in enumerate(input_list):
        for match in re.finditer(r"\d+", line):
            nums[i].append([int(match.group()), match.start(), match.end()])
        for match in re.finditer(r"[^0-9.]", line.strip()):
            syms[i].append([match.group(), match.start()])

    print(solution_1(nums, syms))
    print(solution_2(nums, syms))
