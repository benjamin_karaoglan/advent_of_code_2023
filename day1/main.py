import regex as re
import numpy as np

# Dict to map literals str to corresponding int str
str_to_int = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}


def read_input(input_path: str) -> list:
    """Opens and return an input string as a list containing the lines"""
    with open(input_path, "r") as f:
        input = f.readlines()
    return input


def solution_1(input_list: str) -> int:
    """Part 1 solution"""
    # List comprehension that find for each line of the input all the digits
    # in str format
    parsed_str = ["".join(re.findall(r"\d+", i)) for i in input_list]
    # For each line, maps the first and last caracters, thus digits and forms
    # them into a single string. Then creates a numpy array that converts the
    # str in int
    clean_arr = np.fromiter(
        map(lambda str: int(str[0] + str[-1]), parsed_str), dtype=int
    )
    # Finally sum all the cleaned lines
    return clean_arr.sum()


def solution_2(input_list: str) -> int:
    """Part Two solution"""
    clean_arr = np.array([])
    # We iterate through each line
    for line in input_list:
        # For each line we create an empty dict that we'll use to montior the
        # position of digits
        positioning = {}
        for word, num in str_to_int.items():
            # For key, value elements of our str_to_int dict, we find the
            # first and last occurence positions. We keep the position as key,
            # and the digit under str format but spelled as int as value
            positioning[line.find(word)] = str_to_int[word]
            positioning[line.rfind(word)] = str_to_int[word]
            positioning[line.find(num)] = num
            positioning[line.rfind(num)] = num
        # We delete the key, value pair for key = -1 as that is what is
        # returned when the previously used methods don't find matches
        del positioning[-1]
        # We append the clean int to a numpy array
        clean_arr = np.append(
            clean_arr,
            int(positioning[min(positioning)] + positioning[max(positioning)]),
        )
    # Finally sum all the cleaned lines
    return int(clean_arr.sum())


if __name__ == "__main__":
    input_list = read_input(
        "/home/benjamin_karaoglan/advent_of_code_2023/day1/input.txt"
    )
    print(solution_1(input_list))
    print(solution_2(input_list))
